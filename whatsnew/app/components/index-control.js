import Component from "@ember/component";
import ENV from "whatsnew/config/environment";

export default Component.extend({
  date: ENV.date,
  didInsertElement() {
    Ember.$.getJSON("releases.json").then((resp) => {
      this.set("releases", resp);
    });
  },
  actions: {
    clipSuccess: function () {
      M.toast({ html: "Copied!" });
    },

    clipError: function () {
      M.toast({ html: "Unable to copy!" });
    },
  },

  ubuntu: Ember.computed("selectedRelease", function () {
    let title = this.get("selectedRelease").title;
    return `apt-get install gitlab-ee=${title}-ee.0`;
  }),

  centos: Ember.computed("selectedRelease", function () {
    let title = this.get("selectedRelease").title;
    return `yum install gitlab-ee-${title}`;
  }),

  docker: Ember.computed("selectedRelease", function () {
    let title = this.get("selectedRelease").title;
    return `docker run --rm --name whatsnew gitlab/gitlab-ee:${title}-ee.0`;
  }),

  updates: Ember.computed("selectedRelease", function () {
    let release = this.get("selectedRelease");
    let releases = this.get("releases");
    let list = releases.filter((x) => {
      return compareVersions.compare(x.title, release.title, ">");
    });

    return list;
  }),
});
