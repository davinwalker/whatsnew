#!/usr/bin/env ruby

require 'httparty'

ee_url = 'https://gitlab.com/gitlab-org/gitlab/-/raw/master/CHANGELOG.md'
output = HTTParty.get(ee_url); :ok
result = output.body; :ok

list = result.split(/(^\#{2}\s.*)/); :ok
list.shift # Remove Header

# puts list.inspect
releases = []

loop do
  release = list.shift
  details = list.shift

  title = release.split(' ')[1]
  puts title
  break if Gem::Version.new('9.5.0') > Gem::Version.new(title)
  
  releases.push(title: title, details: details)
  # releases[title] = details
  break if list.empty?
end

File.write('whatsnew/public/releases.json', releases.to_json)
